package a;

import java.util.concurrent.atomic.AtomicBoolean;

public class MyThread implements Runnable {

  int start;

  public MyThread(int start) {
    this.start = start;
  }


  @Override
  public void run() {
    boolean flag = true;
    while (flag) {
      flag = (false);
      for (int i = start; i < start + 50 && !flag; i ++) {
        if (Main.array.get(i) == 0) {
          if (i > 0 && Main.array.get(i - 1) != 0) {
            Main.array.set(i - 1, 0);
            Main.array.set(i, 1);
//            if (i - 1 > 0 && i - 1 < start) {
//              new Thread(new MyThread(start - 50)).start();
//            }
            flag = (true);
          }
        } else {
          if (i < Main.array.size() - 1 && Main.array.get(i + 1) != 1) {
            Main.array.set(i, 0);
            Main.array.set(i + 1, 1);
//            if (i + 1 < Main.array.size() && i + 1 >= start + 50) {
//              new Thread(new MyThread(start + 50)).start();
//            }
            flag = (true);
          }
        }
      }
    }
    Main.lock.lock();
    System.out.println(start);
    for (int i = start; i < start + 50; i ++) {
      System.out.print(Main.array.get(i));
    }
    System.out.println();
    Main.lock.unlock();
  }

}
