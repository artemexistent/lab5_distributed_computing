package a;

import java.util.Random;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

public class Main {

  static Vector<Integer> array = new Vector<>();
  static Random random = new Random();
  static ReentrantLock lock = new ReentrantLock();
  static AtomicBoolean flag = new AtomicBoolean(true);

  public static void main(String[] args) {
    for (int i = 0; i < 100; i ++) {
      array.add(random.nextInt(2));
    }
    System.out.println(array);
    for (int i  = 0; i < array.size(); i += 50) {
      new Thread(new MyThread(i)).start();
    }

  }
}
