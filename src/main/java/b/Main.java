package b;

public class Main {

  static String[] array = new String[] {
      "ABCD",
      "ACCD",
      "ABDD",
      "ABAD"
  };

  public static void main(String[] args) {
    new Thread(new MyString(0)).start();
    new Thread(new MyString(1)).start();
    new Thread(new MyString(2)).start();
    new Thread(new MyString(3)).start();

  }

  public static synchronized boolean check() {
    int k = 0;
    int y = 0;
    for (int i = 0; i < array.length; i ++) {
      int t = 0;
      for (char c : array[i].toCharArray()) {
        if (c == 'A' || c == 'B') {
          t ++;
        }
      }
      if (i == 0) {
        k = t;
      }
      if (k != t) {
        y ++;
      }
      if (y > 1) {
        return false;
      }
    }
    return true;
  }
}
