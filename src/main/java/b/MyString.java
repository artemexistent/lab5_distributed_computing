package b;

import java.util.Random;

public class MyString implements Runnable {

  public int number;
  Random random = new Random();

  public MyString(int number) {
    this.number = number;
  }

  @Override
  public void run() {
    while (!Main.check()) {
      int i = random.nextInt(Main.array[number].length());

      char newC = ' ';

      switch (Main.array[number].toCharArray()[i]) {
        case 'A':
          newC = 'C';
          break;
        case 'B':
          newC = 'D';
          break;
        case 'C':
          newC = 'A';
          break;
        case 'D':
          newC = 'B';
          break;
      }

      if (i == 0) {
        Main.array[number] = newC + Main.array[number].substring(i + 1);
      } else if (i == Main.array[number].length() - 1) {
        Main.array[number] = Main.array[number].substring(0, i) + newC;
      } else {
        Main.array[number] = Main.array[number].substring(0, i) + newC + Main.array[number].substring(i + 1);
      }
    }
    System.out.println(Main.array[number]);
  }
}
