package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var all [3][3]int
var block sync.RWMutex
var r1 *rand.Rand
var finished chan bool

func thread(array int) {
	flag := true
	for flag {
		flag = false
		block.Lock()
		sum := 0
		for i := 0; i < 3; i++ {
			s := 0
			for j := 0; j < 3; j++ {
				s += all[i][j]
			}
			if i == 0 {
				sum = s
			}
			if sum != s {
				flag = true
			}
		}
		block.Unlock()

		block.RLock()

		i := r1.Intn(3)

		all[array][i] += 1

		if r1.Intn(2)%2 == 0 {
			all[array][i] -= 2
		}
		all[array][i] %= 3

		block.RUnlock()
	}
	finished <- true
}

func main() {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 = rand.New(s1)
	i := 1
	for i < 3 {
		all[0][i] = r1.Intn(3)
		all[1][i] = r1.Intn(3)
		all[2][i] = r1.Intn(3)
		i++
	}
	go thread(0)
	go thread(1)
	go thread(2)
	<-finished
	for i = 0; i < 3; i++ {
		s := 0
		for j := 0; j < 3; j++ {
			s += all[i][j]
			fmt.Print(all[i][j])
			fmt.Print(" ")
		}
		fmt.Println(s)
	}
}
